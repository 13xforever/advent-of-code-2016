use std::collections::HashMap;

type TStat = HashMap<char, u32>;

const ALPHABET_SIZE: usize = (b'z' - b'a' + 1) as usize;

fn collect_stats(input: &str) -> Vec<TStat> {
    let lines = input.lines().filter(|l| !l.is_empty()).collect::<Vec<_>>();
    let input_width = lines[0].len();
    let mut hs_list: Vec<TStat> = Vec::with_capacity(input_width);
    for _ in 0..input_width {
        hs_list.push(TStat::with_capacity(ALPHABET_SIZE));
    }
    for l in lines {
        for (i, ch) in l.char_indices() {
            let entry = hs_list[i].entry(ch).or_insert(0);
            *entry += 1;
        }
    }
    hs_list
}

pub fn get_most_frequent(input: &str) -> String {
    let stats = collect_stats(input);
    let result = stats
        .iter()
        .map(|hs| {
            let mut hsts = hs.iter().collect::<Vec<_>>();
            //hsts.sort_unstable_by(|&l, &r| l.0.cmp(r.0));
            hsts.sort_unstable_by(|&l, &r| r.1.cmp(l.1));
            hsts[0].0
        })
        .collect::<String>();
    result
}

pub fn get_least_frequent(input: &str) -> String {
    let stats = collect_stats(input);
    let result = stats
        .iter()
        .map(|hs| {
            let mut hsts = hs.iter().collect::<Vec<_>>();
            //hsts.sort_unstable_by(|&l, &r| l.0.cmp(r.0));
            hsts.sort_unstable_by(|&l, &r| l.1.cmp(r.1));
            hsts[0].0
        })
        .collect::<String>();
    result
}
