#[derive(Copy, Clone)]
struct State<'a> {
    s: &'a str,
    pos: usize,
    decoded_len: usize,
}

fn read_chunk(state: &mut State, recurse: bool, count_only: bool) -> String {
    if state.pos >= state.s.len() {
        return String::new();
    }

    let pos = state.pos;
    let tail = state.s.get(pos..).unwrap();
    let ch = tail.as_bytes().first().unwrap();
    match ch {
        b'(' => {
            let closing_c = tail.find(')').unwrap();
            let compression_s = tail.get(1..closing_c).unwrap();
            let compr_parts = compression_s
                .split('x')
                .map(|p| p.parse::<usize>().unwrap())
                .collect::<Vec<_>>();
            let chunk_len = compr_parts[0];
            let chunk_rep = compr_parts[1];
            let new_tail = tail.get((closing_c + 1)..).unwrap();
            let chunk = new_tail.get(0..chunk_len).unwrap();
            state.pos += closing_c + 1 + chunk_len;
            if recurse {
                let mut chunk_state = State {
                    s: chunk,
                    pos: 0,
                    decoded_len: 0,
                };
                let mut decompressed_chunk = String::new();
                while chunk_state.pos < chunk.len() {
                    let ch_p = read_chunk(&mut chunk_state, true, count_only);
                    if !count_only {
                        decompressed_chunk.push_str(&ch_p);
                    }
                }
                state.decoded_len += chunk_state.decoded_len * chunk_rep;
                if count_only {
                    String::new()
                } else {
                    decompressed_chunk.repeat(chunk_rep)
                }
            } else {
                state.decoded_len += chunk_len * chunk_rep;
                if count_only {
                    String::new()
                } else {
                    chunk.repeat(chunk_rep)
                }
            }
        }
        _ => {
            let next_c = tail.find('(');
            if let Some(np) = next_c {
                state.pos += np;
                state.decoded_len += np;
                if count_only {
                    String::new()
                } else {
                    String::from(tail.get(0..np).unwrap())
                }
            } else {
                state.pos += tail.len();
                state.decoded_len += tail.len();
                if count_only {
                    String::new()
                } else {
                    String::from(tail)
                }
            }
        }
    }
}

pub fn decompress(input: &str, recurse: bool) -> String {
    let mut state = State {
        s: input,
        pos: 0,
        decoded_len: 0,
    };
    let mut result = String::new();
    while state.pos < input.len() {
        let chunk = read_chunk(&mut state, recurse, false);
        result.push_str(&chunk);
    }
    result
}

pub fn get_decompressed_length(input: &str, recurse: bool) -> usize {
    let mut state = State {
        s: input,
        pos: 0,
        decoded_len: 0,
    };
    while state.pos < input.len() {
        read_chunk(&mut state, recurse, true);
    }
    state.decoded_len
}
