extern crate ndarray;
use self::ndarray::{s, Array2};

type TDisplay = Array2<bool>;

fn rect(display: &mut TDisplay, width: usize, height: usize) {
    let (h, w) = display.dim();
    if width > w || height > h {
        panic!(
            "Expected dimensions within {}x{}, but recieved {}x{}",
            w, h, width, height
        );
    }
    display.slice_mut(s![0..height, 0..width]).fill(true);
}

fn rotate_row(display: &mut TDisplay, row: usize, by: usize) {
    let (_, w) = display.dim();
    let old_row = display.row(row).iter().map(|&b| b).collect::<Vec<_>>();
    let mut row = display.row_mut(row);
    for x in 0..w {
        row[(x + by) % w] = old_row[x];
    }
}

fn rotate_col(display: &mut TDisplay, col: usize, by: usize) {
    let (h, _) = display.dim();
    let old_col = display.column(col).iter().map(|&b| b).collect::<Vec<_>>();
    let mut col = display.column_mut(col);
    for y in 0..h {
        col[(y + by) % h] = old_col[y];
    }
}

fn apply(display: &mut TDisplay, cmd: &str) {
    let cmd_parts = cmd.split_ascii_whitespace().collect::<Vec<_>>();
    match cmd_parts[0] {
        "rect" => {
            let a_b = cmd_parts[1]
                .split('x')
                .map(|s| s.parse::<usize>().unwrap())
                .collect::<Vec<_>>();
            rect(display, a_b[0], a_b[1]);
        }
        "rotate" => {
            let idx = cmd_parts[2].split('=').last().unwrap().parse::<usize>().unwrap();
            let by = cmd_parts[4].parse::<usize>().unwrap();
            let (h, w) = display.dim();
            match cmd_parts[1] {
                "row" => rotate_row(display, idx, by % w),
                "column" => rotate_col(display, idx, by % h),
                _ => panic!("Unknown rotate command {}", cmd),
            }
        }
        _ => panic!("Unknown command {}", cmd),
    }
}

fn print_debug(display: &TDisplay) {
    let (h, _) = display.dim();
    for y in 0..h {
        let row = display
            .row(y)
            .iter()
            .map(|&b| if b { '#' } else { ' ' })
            .collect::<String>();
        println!("{}", row);
    }
    println!("")
}

fn replay_cmd_list(input: &str, width: usize, height: usize) -> TDisplay {
    let mut display: TDisplay = TDisplay::from_elem((height, width), false);
    let cmd_list = input.lines().filter(|l| !l.is_empty());
    //println!("Starting from scratch.");
    for l in cmd_list {
        //println!("{}", l);
        apply(&mut display, l);
        //print_debug(&display);
    }
    display
}

pub fn get_lit_px_count(input: &str, width: usize, height: usize) -> usize {
    replay_cmd_list(input, width, height).iter().filter(|&&f| f).count()
}

pub fn print_code(input: &str, width: usize, height: usize) {
    println!("Printing new code:");
    let display = replay_cmd_list(input, width, height);
    print_debug(&display);
}
