extern crate aoc2016;
use aoc2016::day09::*;

fn main() {
        assert_eq!(decompress("(3x3)XYZ", true), "ABBBBBC");
}
