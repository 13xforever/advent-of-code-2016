#[derive(Copy, Clone)]
struct Coord {
    x: i32,
    y: i32,
}

struct Span {
    start: Coord,
    end: Coord,
}

struct State {
    motion_vec: Coord,
    current_coord: Coord,
    path: Vec<Span>,
}

struct Direction {
    rotate_left: bool,
    distance: u32,
}

fn get_direction(dir: &str) -> Direction {
    let rotate_left = dir.as_bytes()[0] == b'L';
    let distance = dir[1..].parse::<u32>().unwrap();
    Direction { rotate_left, distance }
}

fn parse_input(input: &str) -> Vec<Direction> {
    input
        .split_whitespace()
        .map(|s| {
            let s = s.trim_end_matches(',');
            get_direction(s)
        })
        .collect()
}

fn rotate(state: State, rotate_left: bool) -> State {
    let mv = state.motion_vec;
    let new_mv_x = if mv.x == 0 {
        if mv.y == 1 {
            1
        } else {
            -1
        }
    } else {
        0
    };
    let new_mv_y = if mv.y == 0 {
        if mv.x == 1 {
            -1
        } else {
            1
        }
    } else {
        0
    };
    let new_mv_x = if rotate_left { new_mv_x } else { -new_mv_x };
    let new_mv_y = if rotate_left { new_mv_y } else { -new_mv_y };
    State {
        motion_vec: Coord { x: new_mv_x, y: new_mv_y },
        ..state
    }
}

fn move_to(state: State, dist: u32) -> State {
    let dist = dist as i32;
    let new_x_dist = state.current_coord.x + state.motion_vec.x * dist;
    let new_y_dist = state.current_coord.y + state.motion_vec.y * dist;
    State {
        current_coord: Coord {
            x: new_x_dist,
            y: new_y_dist,
        },
        ..state
    }
}

fn generate_path(input: &str) -> State {
    let directions = parse_input(input);
    let mut previous_coord = Coord { x: 0, y: 0 };
    let mut state = State {
        motion_vec: Coord { x: 0, y: -1 },
        current_coord: Coord { x: 0, y: 0 },
        path: Vec::new(),
    };
    let mut path = Vec::with_capacity(directions.len() - 1);
    for d in directions {
        state = rotate(state, d.rotate_left);
        state = move_to(state, d.distance);
        path.push(Span {
            start: previous_coord,
            end: state.current_coord,
        });
        previous_coord = state.current_coord;
    }
    State { path, ..state }
}

pub fn calc_blocks(input: &str) -> u32 {
    let state = generate_path(input);
    (state.current_coord.x.abs() + state.current_coord.y.abs()) as u32
}

fn normalize(span: &Span) -> Span {
    if (span.start.x > span.end.x) || (span.start.y > span.end.y) {
        Span {
            start: span.end,
            end: span.start,
        }
    } else {
        Span { ..*span }
    }
}

fn is_horizontal(span: &Span) -> bool {
    span.start.y == span.end.y
}

fn is_vertical(span: &Span) -> bool {
    span.start.x == span.end.x
}

fn is_a_between_b_hor(a: &Span, b: &Span) -> bool {
    b.start.x <= a.start.x && a.start.x <= b.end.x && a.start.y <= b.start.y && b.start.y <= a.end.y
}

fn is_a_between_b_ver(a: &Span, b: &Span) -> bool {
    b.start.y <= a.start.y && a.start.y <= b.end.y && a.start.x <= b.start.x && b.start.x <= a.end.x
}

fn intersects(span1: &Span, span2: &Span) -> (bool, Coord) {
    let s1 = normalize(span1);
    let s2 = normalize(span2);
    let default = (false, Coord { x: 0, y: 0 });
    if span1.end.x == span2.start.x && span1.end.y == span2.start.y {
        default
    } else if is_vertical(&s1) && is_horizontal(&s2) {
        if is_a_between_b_ver(&s2, &s1) {
            (
                true,
                Coord {
                    x: s1.start.x,
                    y: s2.start.y,
                },
            )
        } else {
            default
        }
    } else if is_horizontal(&s1) && is_vertical(&s2) {
        if is_a_between_b_hor(&s2, &s1) {
            (
                true,
                Coord {
                    x: s2.start.x,
                    y: s1.start.y,
                },
            )
        } else {
            default
        }
    } else if is_vertical(&s1) && is_vertical(&s2) && s1.start.x == s2.start.x {
        if span2.start.y < span2.end.y {
            // goes top to bottom
            if span2.start.y < s1.start.y && span2.end.y >= s1.start.y {
                (true, s1.start)
            } else {
                default
            }
        } else {
            // bottom to top
            if span2.start.y > s1.end.y && span2.end.y <= s1.end.y {
                (true, s1.end)
            } else {
                default
            }
        }
    } else if is_horizontal(&s1) && is_horizontal(&s2) && s1.start.y == s2.start.y {
        if span2.start.x < span2.end.x {
            // goes top to bottom
            if span2.start.x < s1.start.x && span2.end.x >= s1.start.x {
                (true, s1.start)
            } else {
                default
            }
        } else {
            // bottom to top
            if span2.start.x > s1.end.x && span2.end.x <= s1.end.x {
                (true, s1.end)
            } else {
                default
            }
        }
    } else {
        default
    }
}

fn find_first_intersection(input: &str) -> Coord {
    let state = generate_path(input);
    let mut checked = Vec::with_capacity(state.path.len());
    let mut result = state.current_coord;
    let mut found = false;
    for s2 in state.path {
        for s1 in checked.iter() {
            let (does_intersect, intersect_coord) = intersects(&s1, &s2);
            if does_intersect {
                if !found {
                    result = intersect_coord;
                }
                found = true;
                /*
                println!(
                    "({},{})-({},{}) intersects ({},{})-({},{}) in ({}, {})",
                    s2.start.x,
                    s2.start.y,
                    s2.end.x,
                    s2.end.y,
                    s1.start.x,
                    s1.start.y,
                    s1.end.x,
                    s1.end.y,
                    intersect_coord.x,
                    intersect_coord.y
                );
                */
                break;
            }
        }
        if found {
            break;
        }
        checked.push(s2);
    }
    result
}

pub fn calc_intersection(input: &str) -> u32 {
    let intersect_coord = find_first_intersection(input);
    (intersect_coord.x.abs() + intersect_coord.y.abs()) as u32
}
