use std::collections::HashMap;

#[derive(Copy, Clone)]
struct Room<'a> {
    is_valid: bool,
    sector_id: usize,
    name: &'a str,
    checksum: &'a str,
}

const ALPHABET_SIZE: usize = (b'z' - b'a' + 1) as usize;

fn parse_sector_and_hash(id_and_checksum: &str) -> Room {
    let parts = id_and_checksum.trim_end_matches(']').split('[').collect::<Vec<_>>();
    let id = parts[0].parse::<usize>().unwrap();
    let checksum = parts[1];
    Room {
        is_valid: false,
        sector_id: id,
        name: "",
        checksum: checksum,
    }
}

fn parse_room(input: &str) -> Room {
    let parts = input.split('-').collect::<Vec<_>>();
    let last_part = parts[parts.len() - 1];
    let name = input.split_at(input.len() - last_part.len() - 1).0;
    let room = parse_sector_and_hash(last_part);
    let chars = parts.iter().take(parts.len() - 1).map(|&p| p.chars()).flatten();
    let mut char_stats: HashMap<char, u32> = HashMap::with_capacity(ALPHABET_SIZE);
    for ch in chars {
        let counter = char_stats.entry(ch).or_insert(0);
        *counter += 1;
    }
    let mut stats = char_stats.iter().collect::<Vec<_>>();
    stats.sort_unstable_by(|&l, &r| l.0.cmp(r.0));
    stats.sort_by(|&l, &r| r.1.cmp(l.1));
    let hash_chars = stats.iter().take(5).map(|(&c, _)| c).collect::<String>();
    Room {
        is_valid: hash_chars == room.checksum,
        name: name,
        ..room
    }
}

fn decrypt_room(room: &Room) -> String {
    let shift = room.sector_id % ALPHABET_SIZE;
    room.name
        .chars()
        .map(|c| {
            if c == '-' {
                ' '
            } else if 'a' <= c && c <= 'z' {
                (((((((c as u32) - (b'a' as u32)) as usize) + shift) % ALPHABET_SIZE) + (b'a' as usize)) as u8) as char
            } else {
                panic!("Unexpected character {} in room {}", c, room.name)
            }
        })
        .collect::<String>()
}

pub fn is_real_room(input: &str) -> bool {
    parse_room(input).is_valid
}

pub fn real_room_sector_sum(input: &str) -> usize {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| parse_room(l))
        .filter(|&r| r.is_valid)
        .map(|r| r.sector_id)
        .sum()
}

pub fn real_room_name(input: &str) -> String {
    decrypt_room(&parse_room(input))
}

pub fn find_real_room(name: &str, input: &str) -> usize {
    let rooms = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let r = parse_room(l);
            (r, decrypt_room(&r))
        })
        .filter(|(r, _)| r.is_valid)
        .collect::<Vec<_>>();

    for (r, n) in rooms {
        if n == name {
            return r.sector_id;
        }
    }

    panic!("Couldn't find room with the name '{}'", name)
}
