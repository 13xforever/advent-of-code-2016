#[derive(Clone, Copy)]
enum Entity<'a> {
    Chip(&'a str),
    Generator(&'a str),
}

struct State<'a> {
    floors: Vec<Vec<Entity<'a>>>,
}

pub fn count_steps(input: &str) -> usize {
    420
}
