extern crate crypto;

use self::crypto::digest::Digest;
use self::crypto::md5::Md5;

fn to_hex(num: u8) -> char {
    if num < 10 {
        (num + b'0') as char
    } else {
        (num - 10 + b'a') as char
    }
}

pub fn get_password(input: &str, len: usize) -> String {
    let mut md5 = Md5::new();
    let mut hash = [0; 16];
    let mut result = String::new();
    let mut counter = 0;
    while result.len() < len {
        md5.input_str(input);
        let suffix = counter.to_string();
        md5.input_str(&suffix);
        md5.result(&mut hash);
        md5.reset();
        if hash[0] == 0 && hash[1] == 0 && hash[2] < 0x10 {
            let digit = to_hex(hash[2]);
            result.push(digit);
            if result.len() >= len {
                return result;
            }
        }
        counter += 1;
    }
    result
}

pub fn get_password_2(input: &str, len: usize) -> String {
    let mut md5 = Md5::new();
    let mut hash = [0; 16];
    let blank = " ".repeat(len);
    let mut result = blank.as_bytes().to_vec();
    let mut counter = 0;
    while result.iter().any(|&b| b == b' ') {
        md5.input_str(input);
        let suffix = counter.to_string();
        md5.input_str(&suffix);
        md5.result(&mut hash);
        md5.reset();
        if hash[0] == 0 && hash[1] == 0 && hash[2] < 0x10 {
            let idx_position = hash[2] as usize;
            if idx_position < len && result[idx_position] == b' ' {
                let digit = to_hex(hash[3] >> 4) as u8;
                result[idx_position] = digit;
                if result.iter().all(|&b| b > b' ') {
                    return String::from_utf8(result).unwrap();
                }
            }
        }
        counter += 1;
    }
    String::from_utf8(result).unwrap()
}
