#[derive(Copy, Clone)]
struct Triangle {
    x: i32,
    y: i32,
    z: i32,
}

fn parse_triangle(triangle: &str) -> Triangle {
    let triangle_vec: Vec<i32> = triangle
        .split_whitespace()
        .map(|s| s.parse::<i32>().unwrap())
        .collect();
    Triangle {
        x: triangle_vec[0],
        y: triangle_vec[1],
        z: triangle_vec[2],
    }
}

fn is_triangle(t: Triangle) -> bool {
    t.x + t.y > t.z && t.x + t.z > t.y && t.y + t.z > t.x
}

fn transpose(m: &[Triangle]) -> Vec<Triangle> {
    vec![
        Triangle {
            x: m[0].x,
            y: m[1].x,
            z: m[2].x,
        },
        Triangle {
            x: m[0].y,
            y: m[1].y,
            z: m[2].y,
        },
        Triangle {
            x: m[0].z,
            y: m[1].z,
            z: m[2].z,
        },
    ]
}

pub fn count_possible_triangles(input: &str) -> usize {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| parse_triangle(l))
        .filter(|t| is_triangle(*t))
        .count()
}

pub fn count_possible_triangles_vertically(input: &str) -> usize {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| parse_triangle(l))
        .collect::<Vec<_>>()
        .chunks(3)
        .map(|ch| transpose(ch))
        .flatten()
        .filter(|t| is_triangle(*t))
        .count()
}
