type StateUpdate = fn(char, char) -> char;

/*   123
     456
     789   */
fn update_state_normal(current: char, direction: char) -> char {
    match current {
        '1' => match direction {
            'R' => '2',
            'D' => '4',
            _ => current,
        },
        '2' => match direction {
            'L' => '1',
            'R' => '3',
            'D' => '5',
            _ => current,
        },
        '3' => match direction {
            'L' => '2',
            'D' => '6',
            _ => current,
        },
        '4' => match direction {
            'U' => '1',
            'R' => '5',
            'D' => '7',
            _ => current,
        },
        '5' => match direction {
            'L' => '4',
            'U' => '2',
            'R' => '6',
            'D' => '8',
            _ => panic!("Invalid direction {}", direction),
        },
        '6' => match direction {
            'L' => '5',
            'U' => '3',
            'D' => '9',
            _ => current,
        },
        '7' => match direction {
            'U' => '4',
            'R' => '8',
            _ => current,
        },
        '8' => match direction {
            'L' => '7',
            'U' => '5',
            'R' => '9',
            _ => current,
        },
        '9' => match direction {
            'L' => '8',
            'U' => '6',
            _ => current,
        },
        _ => panic!("Invalid current state {}", current),
    }
}

/*     1
     2 3 4
   5 6 7 8 9
     A B C
       D     */
fn update_state_diamond(current: char, direction: char) -> char {
    match current {
        '1' => match direction {
            'D' => '3',
            _ => current,
        },
        '2' => match direction {
            'R' => '3',
            'D' => '6',
            _ => current,
        },
        '3' => match direction {
            'L' => '2',
            'U' => '1',
            'R' => '4',
            'D' => '7',
            _ => panic!("Invalid direction {}", direction),
        },
        '4' => match direction {
            'L' => '3',
            'D' => '8',
            _ => current,
        },
        '5' => match direction {
            'R' => '6',
            _ => current,
        },
        '6' => match direction {
            'L' => '5',
            'U' => '2',
            'R' => '7',
            'D' => 'A',
            _ => panic!("Invalid direction {}", direction),
        },
        '7' => match direction {
            'L' => '6',
            'U' => '3',
            'R' => '8',
            'D' => 'B',
            _ => panic!("Invalid direction {}", direction),
        },
        '8' => match direction {
            'L' => '7',
            'U' => '4',
            'R' => '9',
            'D' => 'C',
            _ => panic!("Invalid direction {}", direction),
        },
        '9' => match direction {
            'L' => '8',
            _ => current,
        },
        'A' => match direction {
            'U' => '6',
            'R' => 'B',
            _ => current,
        },
        'B' => match direction {
            'L' => 'A',
            'U' => '7',
            'R' => 'C',
            'D' => 'D',
            _ => panic!("Invalid direction {}", direction),
        },
        'C' => match direction {
            'L' => 'B',
            'U' => '8',
            _ => current,
        },
        'D' => match direction {
            'U' => 'B',
            _ => current,
        },
        _ => panic!("Invalid state {}", current),
    }
}

fn calc_keypad_button(seq: &str, start_position: char, state_update: StateUpdate) -> char {
    let mut key_code = start_position;
    for c in seq.chars() {
        key_code = state_update(key_code, c);
    }
    key_code
}

fn calc_keypad_code(input: &str, state_update: StateUpdate) -> String {
    let mut result_key_codes: Vec<u8> = Vec::new();
    let mut start_position = '5';
    for l in input.lines() {
        if l.is_empty() {
            continue;
        }

        start_position = calc_keypad_button(l, start_position, state_update);
        result_key_codes.push(start_position as u8);
    }
    String::from_utf8(result_key_codes).unwrap()
}

pub fn calc_keypad_code_normal(input: &str) -> String {
    calc_keypad_code(input, update_state_normal)
}

pub fn calc_keypad_code_diamond(input: &str) -> String {
    calc_keypad_code(input, update_state_diamond)
}
