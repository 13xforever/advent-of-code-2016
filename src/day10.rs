use std::collections::{HashMap, VecDeque};
use std::iter::FromIterator;

pub type TBotId = usize;
pub type TOutputId = usize;
pub type TValue = usize;

#[derive(Debug, Copy, Clone)]
enum CommandTarget {
    Bot(TBotId),
    Output(TOutputId),
}

#[derive(Debug, Copy, Clone)]
enum Command {
    Value(TBotId, TValue),
    Give {
        from: TBotId,
        low: CommandTarget,
        high: CommandTarget,
    },
}

#[derive(Debug)]
struct Bot {
    id: TBotId,
    low: Option<TValue>,
    high: Option<TValue>,
    command_queue: VecDeque<Command>,
}

struct State<F>
where
    F: Fn(&Bot) -> bool,
{
    bots: HashMap<TBotId, Bot>,
    outputs: HashMap<TOutputId, Vec<TValue>>,
    captured_bot_ids: Vec<TBotId>,
    bot_match: F,
}

fn parse_target(target: &str, id: &str) -> CommandTarget {
    match target {
        "bot" => {
            let bot_id = id.parse::<TBotId>().unwrap();
            CommandTarget::Bot(bot_id)
        }
        "output" => {
            let output_id = id.parse::<TOutputId>().unwrap();
            CommandTarget::Output(output_id)
        }
        _ => panic!("Unknown target type {} {}", target, id),
    }
}

fn parse_command(cmd: &str) -> Command {
    let parts = cmd.split_ascii_whitespace().collect::<Vec<_>>();
    match parts[0] {
        "value" => {
            let v = parts[1].parse::<TValue>().unwrap();
            let target = parts.last().unwrap().parse::<TBotId>().unwrap();
            Command::Value(target, v)
        }
        "bot" => {
            let source = parts[1].parse::<TBotId>().unwrap();
            let low_target = parse_target(parts[5], parts[6]);
            let high_target = parse_target(parts[10], parts[11]);
            Command::Give {
                from: source,
                low: low_target,
                high: high_target,
            }
        }
        _ => panic!("Unknown command {}", cmd),
    }
}

fn give<F>(target: CommandTarget, value: TValue, state: &mut State<F>)
where
    F: Fn(&Bot) -> bool,
{
    match target {
        CommandTarget::Output(id) => {
            state
                .outputs
                .entry(id)
                .and_modify(|e| e.push(value))
                .or_insert(vec![value]);
        }
        CommandTarget::Bot(id) => {
            let mut e = state.bots.entry(id).or_insert(Bot {
                id,
                low: None,
                high: None,
                command_queue: VecDeque::new(),
            });
            if e.high.is_some() {
                panic!(
                    "Tried to give bot {} with low {:?} and high {:?} new value {}",
                    e.id, e.low, e.high, value
                );
            } else {
                match e.low {
                    None => {
                        e.low = Some(value);
                    }
                    Some(v) if v <= value => {
                        e.high = Some(value);
                    }
                    Some(_) => {
                        e.high = e.low;
                        e.low = Some(value);
                    }
                }
            };
        }
    }
}

fn execute_cmd<F>(bot_id: TBotId, state: &mut State<F>)
where
    F: Fn(&Bot) -> bool,
{
    if let Some(mut bot) = state.bots.get_mut(&bot_id) {
        if let Some(low_value) = bot.low {
            if let Some(high_value) = bot.high {
                if (state.bot_match)(&bot) {
                    state.captured_bot_ids.push(bot.id);
                }
                if let Some(cmd) = bot.command_queue.pop_front() {
                    bot.low = None;
                    bot.high = None;
                    match cmd {
                        Command::Give {
                            from: _,
                            low: low_target,
                            high: high_target,
                        } => {
                            give(low_target, low_value, state);
                            give(high_target, high_value, state);
                            //println!("Bot {} gave {} to {:?} and {} to {:?}", bot_id, low_value, low_target, high_value, high_target);
                            if let CommandTarget::Bot(id) = low_target {
                                execute_cmd(id, state);
                            };
                            if let CommandTarget::Bot(id) = high_target {
                                execute_cmd(id, state);
                            };
                        }
                        _ => panic!("Unexpected command {:?} in bot's command queue", cmd),
                    }
                }
            }
        }
    } else {
        panic!("Tried to execute command on non-existing bot {}", bot_id);
    }
}

fn simulate<F>(input: &str, match_f: F) -> State<F>
where
    F: Fn(&Bot) -> bool,
{
    let mut result = State {
        bots: HashMap::new(),
        outputs: HashMap::new(),
        captured_bot_ids: Vec::new(),
        bot_match: match_f,
    };
    let cmd_list = input.lines().filter(|l| !l.is_empty()).map(|l| parse_command(l));
    for cmd in cmd_list {
        let bot_id_to_execute = match cmd {
            Command::Value(bot_id, value) => {
                let c = CommandTarget::Bot(bot_id);
                give(c, value, &mut result);
                //println!("Gave {} to {:?} from input", value, c);
                bot_id
            }
            Command::Give { from, .. } => {
                result
                    .bots
                    .entry(from)
                    .and_modify(|e| e.command_queue.push_back(cmd))
                    .or_insert(Bot {
                        id: from,
                        low: None,
                        high: None,
                        command_queue: VecDeque::from_iter(vec![cmd]),
                    })
                    .id
            }
        };
        execute_cmd(bot_id_to_execute, &mut result);
    }
    result
}

pub fn get_bot_with(input: &str, chip1: TValue, chip2: TValue) -> TBotId {
    //println!("New simulation:");
    let result = simulate(input, |b| b.low == Some(chip1) && b.high == Some(chip2));
    //println!("Captured bots: {:?}", result.captured_bot_ids);
    result.captured_bot_ids.first().map(|&i| i).unwrap()
}

pub fn get_output_product(input: &str) -> usize {
    //println!("New simulation:");
    let result = simulate(input, |_| false);
    //println!("Outputs:\n{:?}", result.outputs);
    result
        .outputs
        .iter()
        .filter(|(&id, _)| id == 0 || id == 1 || id == 2)
        .map(|(_, q)| q.get(0).unwrap())
        .fold(1, |acc, v| acc * v)
}
