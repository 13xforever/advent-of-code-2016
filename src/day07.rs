use std::collections::HashSet;

pub fn is_valid_tls(address: &str) -> bool {
    let address_bytes = address.as_bytes();
    let address_len = address_bytes.len();
    if address_len < 4 {
        return false;
    }

    let mut has_abba = false;
    let mut inside_brackets = false;
    for i in 0..(address_len - 3) {
        let c = address_bytes[i];
        if c == b'[' {
            inside_brackets = true;
        } else if c == b']' {
            inside_brackets = false;
        } else if c != address_bytes[i + 1] && address_bytes[i + 1] == address_bytes[i + 2] && address_bytes[i + 3] == c
        {
            if inside_brackets {
                return false;
            }
            has_abba = true;
        }
    }
    has_abba
}

pub fn is_valid_ssl(address: &str) -> bool {
    let address_bytes = address.as_bytes();
    let address_len = address_bytes.len();
    if address_len < 8 {
        return false;
    }

    let mut aba: HashSet<String> = HashSet::new();
    let mut expected_aba: HashSet<String> = HashSet::new();
    let mut inside_brackets = false;
    for i in 0..(address_len - 2) {
        let c = address_bytes[i];
        if c == b'[' {
            inside_brackets = true;
        } else if c == b']' {
            inside_brackets = false;
        } else if c != address_bytes[i + 1] && address_bytes[i + 2] == c {
            if inside_brackets {
                let c1 = c as char;
                let c2 = address_bytes[i + 1] as char;
                let expected = format!("{0}{1}{0}", c2, c1);
                expected_aba.insert(expected);
            } else {
                let s = String::from(address.get(i..i + 3).unwrap());
                aba.insert(s);
            }
        }
    }
    expected_aba.intersection(&aba).count() > 0
}

pub fn valid_tls_sum(input: &str) -> usize {
    input.lines().filter(|l| !l.is_empty() && is_valid_tls(l)).count()
}

pub fn valid_ssl_sum(input: &str) -> usize {
    input.lines().filter(|l| !l.is_empty() && is_valid_ssl(l)).count()
}
