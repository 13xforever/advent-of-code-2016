#[cfg(test)]
mod tests11 {
    extern crate aoc2016;
    use self::aoc2016::day11::*;

    #[test]
    fn part1() {
        assert_eq!(count_steps(TEST_INPUT), 11);
        assert_eq!(count_steps(REAL_INPUT), 42);
    }

    #[test]
    fn part2() {}

    const TEST_INPUT: &str = "
The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
The second floor contains a hydrogen generator.
The third floor contains a lithium generator.
The fourth floor contains nothing relevant.
";

    const REAL_INPUT: &str = "
The first floor contains a promethium generator and a promethium-compatible microchip.
The second floor contains a cobalt generator, a curium generator, a ruthenium generator, and a plutonium generator.
The third floor contains a cobalt-compatible microchip, a curium-compatible microchip, a ruthenium-compatible microchip, and a plutonium-compatible microchip.
The fourth floor contains nothing relevant.
";
}
