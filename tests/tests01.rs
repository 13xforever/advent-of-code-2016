#[cfg(test)]
mod tests01 {
    extern crate aoc2016;
    use self::aoc2016::day01::*;

    #[test]
    fn part1() {
        assert_eq!(calc_blocks("R2, L3"), 5);
        assert_eq!(calc_blocks("R2, R2, R2"), 2);
        assert_eq!(calc_blocks("R5, L5, R5, R3"), 12);
        assert_eq!(calc_blocks(REAL_INPUT), 279);
    }

    #[test]
    fn part2() {
        assert_eq!(calc_intersection("R8, R4, R4, R8"), 4);
        assert_eq!(calc_intersection(REAL_INPUT), 163);
    }

    const REAL_INPUT: &str = "L4, L1, R4, R1, R1, L3, R5, L5, L2, L3, R2, R1, L4, R5, R4, L2, R1, R3, L5, R1, L3, L2, R5, L4, L5, R1, R2, L1, R5, L3, R2, R2, L1, R5, R2, L1, L1, R2, L1, R1, L2, L2, R4, R3, R2, L3, L188, L3, R2, R54, R1, R1, L2, L4, L3, L2, R3, L1, L1, R3, R5, L1, R5, L1, L1, R2, R4, R4, L5, L4, L1, R2, R4, R5, L2, L3, R5, L5, R1, R5, L2, R4, L2, L1, R4, R3, R4, L4, R3, L4, R78, R2, L3, R188, R2, R3, L2, R2, R3, R1, R5, R1, L1, L1, R4, R2, R1, R5, L1, R4, L4, R2, R5, L2, L5, R4, L3, L2, R1, R1, L5, L4, R1, L5, L1, L5, L1, L4, L3, L5, R4, R5, R2, L5, R5, R5, R4, R2, L1, L2, R3, R5, R5, R5, L2, L1, R4, R3, R1, L4, L2, L3, R2, L3, L5, L2, L2, L1, L2, R5, L2, L2, L3, L1, R1, L4, R2, L4, R3, R5, R3, R4, R1, R5, L3, L5, L5, L3, L2, L1, R3, L4, R3, R2, L1, R3, R1, L2, R4, L3, L3, L3, L1, L2";
}
