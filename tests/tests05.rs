#[cfg(test)]
mod tests05 {
    extern crate aoc2016;
    use self::aoc2016::day05::*;

    #[test]
    fn part1() {
        assert_eq!(get_password(TEST_INPUT, 8), "18f47a30");
        assert_eq!(get_password(REAL_INPUT, 8), "d4cd2ee1");
    }

    #[test]
    fn part2() {
        assert_eq!(get_password_2(TEST_INPUT, 8), "05ace8e3");
        assert_eq!(get_password_2(REAL_INPUT, 8), "f2c730e5");
    }

    const TEST_INPUT: &str = "abc";
    const REAL_INPUT: &str = "ugkcyxxp";
}
