#[cfg(test)]
mod tests08 {
    extern crate aoc2016;
    use self::aoc2016::day08::*;

    #[test]
    fn part1() {
        assert_eq!(get_lit_px_count("rect 3x2", 7, 3), 6);
        assert_eq!(get_lit_px_count(TEST_INPUT, 7, 3), 6);
        assert_eq!(get_lit_px_count(TEST_AOC, 70, 6), 152);
        assert_eq!(get_lit_px_count(REAL_INPUT, 50, 6), 106);
    }

    #[test]
    fn part2() {
        print_code(TEST_AOC, 70, 6);
        print_code(REAL_INPUT, 50, 6);
    }

    const TEST_INPUT: &str = "
rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1
";

    const REAL_INPUT: &str = "
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 3
rect 2x1
rotate row y=0 by 2
rect 1x2
rotate row y=1 by 5
rotate row y=0 by 3
rect 1x2
rotate column x=30 by 1
rotate column x=25 by 1
rotate column x=10 by 1
rotate row y=1 by 5
rotate row y=0 by 2
rect 1x2
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=2 by 18
rotate row y=0 by 5
rotate column x=0 by 1
rect 3x1
rotate row y=2 by 12
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate column x=20 by 1
rotate row y=2 by 5
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=2 by 15
rotate row y=0 by 15
rotate column x=10 by 1
rotate column x=5 by 1
rotate column x=0 by 1
rect 14x1
rotate column x=37 by 1
rotate column x=23 by 1
rotate column x=7 by 2
rotate row y=3 by 20
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=3 by 5
rotate row y=2 by 2
rotate row y=1 by 4
rotate row y=0 by 4
rect 1x4
rotate column x=35 by 3
rotate column x=18 by 3
rotate column x=13 by 3
rotate row y=3 by 5
rotate row y=2 by 3
rotate row y=1 by 1
rotate row y=0 by 1
rect 1x5
rotate row y=4 by 20
rotate row y=3 by 10
rotate row y=2 by 13
rotate row y=0 by 10
rotate column x=5 by 1
rotate column x=3 by 3
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate row y=4 by 10
rotate row y=3 by 10
rotate row y=1 by 10
rotate row y=0 by 10
rotate column x=7 by 2
rotate column x=5 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate row y=4 by 20
rotate row y=3 by 12
rotate row y=1 by 15
rotate row y=0 by 10
rotate column x=8 by 2
rotate column x=7 by 1
rotate column x=6 by 2
rotate column x=5 by 1
rotate column x=3 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate column x=46 by 2
rotate column x=43 by 2
rotate column x=24 by 2
rotate column x=14 by 3
rotate row y=5 by 15
rotate row y=4 by 10
rotate row y=3 by 3
rotate row y=2 by 37
rotate row y=1 by 10
rotate row y=0 by 5
rotate column x=0 by 3
rect 3x3
rotate row y=5 by 15
rotate row y=3 by 10
rotate row y=2 by 10
rotate row y=0 by 10
rotate column x=7 by 3
rotate column x=6 by 3
rotate column x=5 by 1
rotate column x=3 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate column x=19 by 1
rotate column x=10 by 3
rotate column x=5 by 4
rotate row y=5 by 5
rotate row y=4 by 5
rotate row y=3 by 40
rotate row y=2 by 35
rotate row y=1 by 15
rotate row y=0 by 30
rotate column x=48 by 4
rotate column x=47 by 3
rotate column x=46 by 3
rotate column x=45 by 1
rotate column x=43 by 1
rotate column x=42 by 5
rotate column x=41 by 5
rotate column x=40 by 1
rotate column x=33 by 2
rotate column x=32 by 3
rotate column x=31 by 2
rotate column x=28 by 1
rotate column x=27 by 5
rotate column x=26 by 5
rotate column x=25 by 1
rotate column x=23 by 5
rotate column x=22 by 5
rotate column x=21 by 5
rotate column x=18 by 5
rotate column x=17 by 5
rotate column x=16 by 5
rotate column x=13 by 5
rotate column x=12 by 5
rotate column x=11 by 5
rotate column x=3 by 1
rotate column x=2 by 5
rotate column x=1 by 5
rotate column x=0 by 1
";

    const TEST_AOC: &str = "
rect 70x2
rotate column x=69 by 3
rotate row y=3 by 1
rotate column x=68 by 2
rotate column x=67 by 5
rotate column x=66 by 5
rotate row y=5 by 69
rotate column x=64 by 5
rotate row y=2 by 66
rotate column x=63 by 1
rotate row y=3 by 67
rotate column x=62 by 2
rotate row y=5 by 68
rotate column x=61 by 5
rotate row y=5 by 1
rotate column x=59 by 5
rotate row y=2 by 68
rotate column x=58 by 1
rotate row y=3 by 67
rotate row y=4 by 60
rotate column x=57 by 3
rotate row y=5 by 68
rotate column x=56 by 5
rotate column x=54 by 3
rotate row y=2 by 68
rotate column x=53 by 1
rotate row y=5 by 67
rotate column x=52 by 4
rotate row y=5 by 2
rotate column x=51 by 5
rotate column x=49 by 3
rotate row y=2 by 67
rotate row y=3 by 9
rotate column x=48 by 2
rotate row y=5 by 67
rotate column x=47 by 4
rotate row y=4 by 2
rotate column x=46 by 3
rotate row y=3 by 2
rotate column x=45 by 2
rotate row y=3 by 7
rotate row y=4 by 8
rotate column x=44 by 3
rotate row y=2 by 3
rotate row y=3 by 2
rotate column x=43 by 2
rotate row y=5 by 69
rotate column x=42 by 5
rotate column x=41 by 5
rotate row y=5 by 9
rotate column x=39 by 5
rotate row y=2 by 3
rotate column x=38 by 1
rotate row y=3 by 66
rotate row y=4 by 65
rotate column x=37 by 3
rotate column x=36 by 5
rotate row y=2 by 67
rotate row y=3 by 69
rotate column x=34 by 2
rotate row y=4 by 69
rotate row y=5 by 68
rotate column x=33 by 4
rotate row y=2 by 69
rotate row y=3 by 3
rotate column x=32 by 2
rotate row y=3 by 1
rotate row y=4 by 6
rotate column x=31 by 3
rotate row y=3 by 1
rotate column x=30 by 2
rotate row y=3 by 2
rotate row y=4 by 1
rotate column x=29 by 3
rotate row y=5 by 4
rotate column x=28 by 5
rotate row y=5 by 1
rotate column x=26 by 5
rotate row y=5 by 3
rotate column x=25 by 5
rotate column x=23 by 5
rotate row y=2 by 65
rotate row y=3 by 66
rotate column x=22 by 2
rotate row y=2 by 7
rotate column x=21 by 1
rotate row y=2 by 2
rotate column x=20 by 1
rotate row y=5 by 67
rotate column x=19 by 5
rotate row y=4 by 60
rotate column x=18 by 4
rotate column x=17 by 5
rotate row y=5 by 2
rotate column x=16 by 5
rotate row y=5 by 2
rotate column x=13 by 5
rotate row y=2 by 64
rotate row y=3 by 62
rotate column x=12 by 2
rotate row y=4 by 66
rotate column x=11 by 3
rotate column x=10 by 1
rotate row y=5 by 67
rotate column x=9 by 5
rotate row y=2 by 1
rotate column x=8 by 1
rotate row y=3 by 67
rotate column x=7 by 2
rotate row y=5 by 68
rotate column x=6 by 5
rotate column x=4 by 5
rotate row y=2 by 67
rotate column x=3 by 1
rotate row y=3 by 66
rotate row y=4 by 66
rotate column x=2 by 3
rotate column x=1 by 5
rotate row y=2 by 68
rotate row y=3 by 68
rotate row y=5 by 69
rect 1x6
rotate row y=2 by 4
rotate row y=4 by 2
rect 1x6
rotate row y=2 by 2
rotate row y=4 by 3
rect 1x6
rotate row y=2 by 3
rotate row y=4 by 3
rect 1x6
rotate row y=2 by 2
rotate row y=4 by 2
rect 1x6
rotate row y=2 by 3
rotate row y=4 by 3
rect 1x6
rotate row y=0 by 1
";
}
